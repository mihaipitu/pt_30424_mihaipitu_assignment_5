package Model;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class OutputTextFile {
	File outputFile = new File("Information.txt");
	public OutputTextFile()
	{
		setOutputToFile();
	}
	
	public void setOutputToFile()
	{
		try{
			if(!outputFile.exists())
			{
				outputFile.createNewFile();
			}
			PrintStream output = new PrintStream(outputFile);
			System.setOut(output);
		}catch(IOException e){
			System.out.println(OutputTextFile.class.getName());
		}
	}
}
