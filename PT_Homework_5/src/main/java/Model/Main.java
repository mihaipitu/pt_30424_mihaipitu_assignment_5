package Model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {

	public static void main(String args[])
	{
		DataReadFromFile data = new DataReadFromFile();
		List<MonitoredData> monitoredData = data.getActivities();
		ArrayList<Integer> activityDays = new ArrayList<Integer>();
		List<String> activityList = new ArrayList<String>();
		OutputTextFile output = new OutputTextFile();
		Map<Integer,ArrayList<String>> activitiesDaily = new TreeMap<Integer,ArrayList<String>>();
		Map<String,Long> timedIntervalActivities = new TreeMap<String,Long>();
		Map<String,Integer> filterIntervalActivities = new TreeMap<String,Integer>();
		Map<Integer,Map<String,Long>> dailyActivities = new TreeMap<Integer,Map<String,Long>>();
		Comparator<Long> longCompare = (a,b) -> a>=b;
		Comparator<Double> doubleCompare = (a,b) -> a>=b;
		for(MonitoredData mon: monitoredData)
		{
			if(!activityDays.contains(mon.getStartTime().getDayOfMonth()))
			{
				activityDays.add(mon.getStartTime().getDayOfMonth());
			}
			else
			{
				ArrayList<String> stringList = activitiesDaily.get(mon.getStartTime().getDayOfMonth());
				if(stringList == null)
					stringList = new ArrayList<String>();
				stringList.add(mon.getActivity());
				activitiesDaily.put(mon.getStartTime().getDayOfMonth(), stringList);
			}
			activityList.add(mon.getActivity());
		}
		
		System.out.println("Number of days: "+activityDays.size());
		System.out.println(" ");
		Map<String, Long> groupActivities = activityList.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
		Set activities = groupActivities.entrySet();
		Iterator iter = activities.iterator();
		System.out.println("The activities grouped: ");
		while(iter.hasNext())
		{
			Map.Entry entry = (Map.Entry) iter.next();
			System.out.println("	"+entry.getKey() + "    " + entry.getValue());
		}
		System.out.println(" ");
		System.out.println("The activities grouped by day:");
		Set days = activitiesDaily.entrySet();
		Iterator i = days.iterator();
		while(i.hasNext())
		{
			Map.Entry entry = (Map.Entry) i.next();
			List<String> dayData = (List<String>) entry.getValue();
			Map<String,Long> dataTaken = dayData.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
			dailyActivities.put((Integer) entry.getKey(), dataTaken);
		}
		
		Set dailySet = dailyActivities.entrySet();
		Iterator iterate = dailySet.iterator();
		while(iterate.hasNext())
		{
			Map.Entry entry = (Map.Entry) iterate.next();
			System.out.println("	The day " + entry.getKey() + " has the following activities done");
			Map<String,Long> dailyInfo = (Map<String, Long>) entry.getValue();
			Set day = dailyInfo.entrySet();
			Iterator it = day.iterator();
			while(it.hasNext())
			{
				Map.Entry en = (Map.Entry) it.next();
				System.out.println("		"+en.getKey() + "    " + en.getValue());
			}
			System.out.println(" ");
		}
		for(MonitoredData mon: monitoredData)
		{
			long startTimeInterval = mon.getStartTime().getHour()*3600 + mon.getStartTime().getMinute()*60 + mon.getStartTime().getSecond();
			long endTimeInterval = mon.getEndTime().getHour()*3600 + mon.getEndTime().getMinute()*60 + mon.getEndTime().getSecond();
			if(mon.getEndTime().getMonth().getValue()>mon.getStartTime().getMonth().getValue())
			{
				endTimeInterval *= 24*3600;
			}
			if(mon.getEndTime().getDayOfMonth() > mon.getStartTime().getDayOfMonth())
			{
				endTimeInterval *= 24*3600;
			}
			long x;
			long y = endTimeInterval - startTimeInterval;
			int noOccurence;
			if(timedIntervalActivities.get(mon.getActivity()) == null)
				x =0;
			else
				x = timedIntervalActivities.get(mon.getActivity());
			x += y;
			
			if(!longCompare.compare(y, (long) 300))
			{
				if(filterIntervalActivities.get(mon.getActivity())== null)
				{
					noOccurence = 0;
				}
				else
				{
					noOccurence = filterIntervalActivities.get(mon.getActivity());
				}
				noOccurence++;
				filterIntervalActivities.put(mon.getActivity(), noOccurence);
			}
			timedIntervalActivities.put(mon.getActivity(), x);
		}
		System.out.println(" ");
		System.out.println("The activities that have a total time greater than 10 hours: ");
		System.out.println(" ");
		Set timeActivities = timedIntervalActivities.entrySet();
		Iterator en = timeActivities.iterator();
		while(en.hasNext())
		{
			Map.Entry mapEn = (Map.Entry) en.next();
			if(longCompare.compare((long)mapEn.getValue(), (long) 36000))
			{
				System.out.println(mapEn.getKey()+ "   " + (long)mapEn.getValue()/3600 + ":" + ((long)mapEn.getValue()%3600)/60 + ":" +  ((long)mapEn.getValue()%3600)%60);
			}
		}
		
		Set filteredActivities = filterIntervalActivities.entrySet();
		Iterator iteration = filteredActivities.iterator();
		List<String> activityFiltered = new ArrayList<String>();
		while(iteration.hasNext())
		{
			Map.Entry entry = (Map.Entry) iteration.next();
			int value = (int) entry.getValue();
			
			double x = (double) value;
			long activ = 0;
			Iterator it = activities.iterator();
			boolean test = true;
			while(it.hasNext() && test == true)
			{
				Map.Entry en2 = (Map.Entry) it.next();
				if((String)en2.getKey() == (String)entry.getKey())
				{
					activ =(long) en2.getValue();
					test = false;
				}
			}
			double y = (double) activ;
			if(doubleCompare.compare(x/y, (Double) 0.9) && y>0)
			{
				activityFiltered.add((String) entry.getKey());
			}
		}
		int j;
		System.out.println(" ");
		System.out.println("The activities that 90% of them have duration less than 5 minutes: ");
		System.out.println(" ");
		for(j=0;j<activityFiltered.size();j++)
		{
			System.out.println(activityFiltered.get(j));
		}
	}
}
