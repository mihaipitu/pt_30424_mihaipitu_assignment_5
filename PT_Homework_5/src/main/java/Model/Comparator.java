package Model;

public interface Comparator<T> {
	public boolean compare(T a, T b);
}
