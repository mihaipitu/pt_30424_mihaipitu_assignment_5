package Model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DataReadFromFile {

	public ArrayList<MonitoredData> getActivities() 
	{
		ArrayList<MonitoredData> monitoredData = new ArrayList<MonitoredData>();
		String line;
		try 
		{
		    FileInputStream fis = new FileInputStream("Activity.txt");
		    InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
		    BufferedReader br = new BufferedReader(isr);
		    while ((line = br.readLine()) != null) 
		    {
		        String[] splitLine = line.split("		");
		        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		        LocalDateTime startTime = LocalDateTime.parse(splitLine[0], formatter);
		        LocalDateTime endTime = LocalDateTime.parse(splitLine[1], formatter);
		        MonitoredData monitor = new MonitoredData(startTime,endTime,splitLine[2]);
		        monitoredData.add(monitor);
		    }
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		return monitoredData;
	}
}
